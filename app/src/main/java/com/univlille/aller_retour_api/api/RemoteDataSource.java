package com.univlille.aller_retour_api.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteDataSource {
    private static BookDisplayService bookDisplayService;
    private static Retrofit retrofit;

    public static BookDisplayService getBookDisplayService() {
        if (bookDisplayService == null) {
            bookDisplayService = getRetrofit().create(BookDisplayService.class);
        }
        return bookDisplayService;
    }

    public static Retrofit getRetrofit() {
        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://www.googleapis.com/books/v1/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
