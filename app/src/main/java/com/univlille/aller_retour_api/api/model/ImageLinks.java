package com.univlille.aller_retour_api.api.model;

public class ImageLinks {

    private String thumbnail;
    private String smallThumbnail;

    public String getThumbnail() {
        return thumbnail;
    }

    public String getSmallThumbnail() {
        return smallThumbnail;
    }
}
