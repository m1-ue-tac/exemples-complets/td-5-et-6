package com.univlille.aller_retour_api;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.univlille.aller_retour_api.api.BookDisplayService;
import com.univlille.aller_retour_api.api.RemoteDataSource;
import com.univlille.aller_retour_api.api.model.Book;
import com.univlille.aller_retour_api.api.model.BookSearchResponse;

import java.util.Iterator;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    // Ceci est MA clé, merci de ne pas l'utiliser et de mettre la vôtre à la place !
    public static final String API_KEY = "....";
    public BookDisplayService service;
    private TextView txt_Result;
    private EditText keyword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_Result = findViewById(R.id.txt_Result);
        keyword = findViewById(R.id.keyword);

        Button btn_search = findViewById(R.id.btn_search);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testAPI();
            }
        });

    }

    // Second test de l'appel de l'API. On lance ne recherche de livre avec le mot-clé saisi (par défaut "name")
    // et on affiche le résultat dans la zone texte sous le bouton
    public void testAPI() {
        // on crée le service Retrofit qui va nous permettre d'utliser l'API
        service = RemoteDataSource.getBookDisplayService();
        // puis on appelle ce service pour lancer la requête de recherche de livres. Cette requête
        // renvoie un flux RxJava de type Single<BookSearchResponse>.
        // Sur ce flux, on 'branche' un observer (subscriber) qui va gérer les données émises par
        // le flux, en l'occurrence ici soit l'item renvoyé (ici une liste de livres), soit une
        // erreur. C'est le comportement du flux Single (cf. doc officielle)
        SingleObserver singleObserver = service.searchBooks(keyword.getText().toString(), API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BookSearchResponse>() {

                    // si tout s'est bien passé, on a récupéré l'item. Il n'y a plus qu'à l'afficher
                    public void onSuccess(BookSearchResponse bookSearchResponse) {
                        String resultat = new String();
                        String newligne = System.getProperty("line.separator");
                        List<Book> bookList = bookSearchResponse.getBookList();
                        for (Iterator iter = bookList.iterator(); iter.hasNext(); ) {
                            Book ch2 = (Book) iter.next();
                            //Log.d("JC", ch2.getVolumeInfo().getTitle());
                            resultat = resultat.concat(ch2.getVolumeInfo().getTitle());
                            resultat = resultat.concat(newligne);
                        }
                        txt_Result.setText(resultat);
                    }

                    // si erreur dans le fux, on affiche l'erreur
                    @Override
                    public void onError(Throwable e) {
                        Log.d("JC", "erreur : " + e.getMessage());
                    }
                });
    }
}